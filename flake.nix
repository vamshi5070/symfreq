{
  description = "symfreq";



  inputs = {
    nixpkgs.url = github:nixos/nixpkgs/nixos-22.05;
    flake-utils.url = github:numtide/flake-utils;
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }: let
    utils = flake-utils.lib;
  in
  utils.eachDefaultSystem (system: let
    compilerVersion = "ghc902";
    pkgs = nixpkgs.legacyPackages.${system};
    hsPkgs = pkgs.haskell.packages.${compilerVersion}.override {
      overrides = hfinal: hprev: {
        symfreq = hfinal.callCabal2nix "symfreq" ./. {};
      };
    };
  in rec {
    packages =
      utils.flattenTree
      {symfreq = hsPkgs.symfreq;};

      devShell = hsPkgs.shellFor {
        withHoogle = true;
        packages = p: [
          p.symfreq
        ];
        buildInputs = [
          # pkgs.cabal2nix
          pkgs.cabal-install
          #          pkgs.postgresql
          pkgs.hlint
          pkgs.ormolu
          pkgs.haskell.compiler."${compilerVersion}"
          hsPkgs.ghcid

          # pkgs.qemu
          # pkgs.purescript
          # hsPkgs.haskell-language-server
          # hsPkgs.alex
          # hsPkgs.happy
          # hsPkgs.hspec-discover
        ];
        # ++ (builtins.attrValues (import ./scripts.nix {s = pkgs.writeShellScriptBin;}));
      };

      defaultPackage = packages.symfreq;
  });
}
