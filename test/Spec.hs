import qualified Data.Map as Map
import Test.Hspec
import Code

main :: IO()
main = hspec $ do
  describe "extractSymbols" $ do
    it "returns symbol from string" $ do
      extractSymbols "a%dssd&#" `shouldBe` "%&#" 

  describe "countSymbolFreq" $ do
    it "counts symbol occurance frequency" $ do
      (countSymbolFreq . extractSymbols) "a %%dssd&#" `shouldBe` Map.fromList [('%',2),('&',1),('#',1)]
