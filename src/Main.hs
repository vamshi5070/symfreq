module Main where

-- import Data.Char
import Code
import qualified Data.Map as M
import System.Environment
import Data.List (sortBy)
import Control.Monad (forM_)
-- extractSymbols :: String -> [Char]
-- extractSymbols = filter (not . isSymbol)

-- isSymbol = not isAlphaNum
-- isSymbol 

formatSymfreq :: [(Char,Int)] -> String
formatSymfreq = unlines . map formatFreq
  where formatFreq (sym,count) = sym : (" -> " <> show count)


sortSymDesc :: [(Char,Int)] -> [(Char,Int)]
sortSymDesc = sortBy (\(_,x) (_,y) -> compare y x)

test :: String
test = "a %%dssd&#"

main :: IO ()
main = do
  args <- getArgs
  case args of
    [] -> error "empty args !!"
    fps -> forM_ fps myFunc



myFunc :: FilePath -> IO ()
myFunc fp = do
  putStrLn $ fp <> " :   "
  content <- readFile fp    
  putStrLn $ formatSymfreq $ (sortSymDesc .  M.toList . countSymbolFreq . extractSymbols) content
--
  --print "Rgv"
  
  -- putStrLn "porn"
